#utility functions

#version 20200318

#find package name from target or prerequisite name
define package_name
echo $(@F) | sed -r 's/-[[:digit:]]+$$//'
endef

#exit recipe gracefully if target file exists
define exit?
if [ -e tmp/$(@F) ]; then exit 0; fi
endef

#find archive name from package name in package download urls file
define find_archive
basename $$(grep "^$$($(package_name))[[:blank:]]" $(urls) | cut -f 2)
endef

#find archive name from package name in package download urls file
#arg1: package name
define find_archive_func
basename $$(grep "^$(1)[[:blank:]]" $(urls) | cut -f 2)
endef

#find tar archive root directory name from package name in sources working directory
define tar_root
tar -tf $(wdir)/sources/$$($(find_archive)) | sed '2q;d' | cut -d '/' -f 1
endef

#find tar archive root directory name from package name in sources working directory
#arg1: package name
define tar_root_func
tar -tf $(wdir)/sources/$$($(call find_archive_func,$(1))) | sed '2q;d' | cut -d '/' -f 1
endef

#cd to sources working directory, decompress, untar package and cd to root directory of the archive
define extract
$(exit?)
pushd $(wdir)/sources
tar -xvf $$($(find_archive))
pushd $$($(tar_root))
endef

#cd to sources working directory, decompress, untar package and cd to root directory of the archive
#arg1: package name
define extract_func
pushd $(wdir)/sources
tar -xvf $$($(call find_archive_func,$(1)))
pushd $$($(call tar_root_func,$(1)))
endef

#exit extracted package directory and remove it
define remove
cd ~-0 #change to directory at the bottom of the stack
dirs -c #clear the directory stack
rm -rvf $(wdir)/sources/$$($(tar_root))
endef

#exit extracted package directory and remove it
#arg1: package name
define remove_func
cd ~-0 #change to directory at the bottom of the stack
dirs -c #clear the directory stack
rm -rvf $(wdir)/sources/$$($(call tar_root_func,$(1)))
endef
