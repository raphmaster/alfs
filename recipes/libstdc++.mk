#libstdc++

#version 9.3.0

libstdc++ : glibc-1
	@$(call extract_func,gcc)
	mkdir -pv build
	pushd build 
	../libstdc++-v3/configure --host=$$LFS_TGT --prefix=/tools --disable-multilib --disable-nls --disable-libstdcxx-threads --disable-libstdcxx-pch --with-gxx-include-dir=/tools/$${LFS_TGT}/include/c++/9.3.0
	$(MAKE)
	$(MAKE) install
	$(call remove_func,gcc)
	touch tmp/$(@F)
