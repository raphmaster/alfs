#gcc pass 1

#version 9.3.0

gcc-1 : binutils-1
	@$(extract)
	#unpack mpfr,gmp,mpc and rename them so gcc build will use them
	tar -xvf ../$$($(call find_archive_func,mpfr))
	mv -vf $$($(call tar_root_func,mpfr)) mpfr
	tar -xvf ../$$($(call find_archive_func,gmp))
	mv -vf $$($(call tar_root_func,gmp)) gmp
	tar -xvf ../$$($(call find_archive_func,mpc))
	mv -vf $$($(call tar_root_func,mpc)) mpc
	#change default dynamic linker to the one in tools
	#remove /usr/include from include search path
	for file in gcc/config/{linux,i386/linux{,64}}.h
	do
	    cp -v $${file}{,.orig} && #backup files
	    sed -i -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' -e 's@/usr@/tools@g' $$file && #prepend /tools to /lib/ld, /lib64/ld and /lib32/ld and replace /usr with /tools
	    echo '
	#undef STANDARD_STARTFILE_PREFIX_1
	#undef STANDARD_STARTFILE_PREFIX_2
	#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
	#define STANDARD_STARTFILE_PREFIX_2 ""' >> $$file || exit 1
	done
	sed -i -e '/m64=/s/lib64/lib/' gcc/config/i386/t-linux64 #change 64-bit libraries default directory to lib
	#official documentation recommends building in a dedicated directory
	mkdir -pv build
	pushd build
	../configure --target=$$LFS_TGT --prefix=/tools --with-glibc-version=2.11 --with-sysroot=$(wdir) --with-newlib --without-headers --with-local-prefix=/tools --with-native-system-header-dir=/tools/include --disable-nls --disable-shared --disable-multilib --disable-decimal-float --disable-threads --disable-libatomic --disable-libgomp --disable-libquadmath --disable-libssp --disable-libvtv --disable-libstdcxx --enable-languages=c,c++
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
