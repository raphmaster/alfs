#diffutils pass 1

#version 3.7

diffutils-1 : coreutils-1
	@$(extract)
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
