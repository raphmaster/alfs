#gawk pass 1

#version 5.0.1

gawk-1 : findutils-1
	@$(extract)
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
