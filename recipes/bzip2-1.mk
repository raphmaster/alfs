#bzip2 pass 1

#version 1.0.8

bzip2-1 : bison-1
	@$(extract)
	$(MAKE) -f Makefile-libbz2_so
	$(MAKE) clean
	$(MAKE)
	$(MAKE) PREFIX=/tools install
	cp -v bzip2-shared /tools/bin/bzip2
	cp -av libbz2.so* /tools/lib
	ln -sv libbz2.so.1.0 /tools/lib/libbz2.so
	$(remove)
	touch tmp/$(@F)
