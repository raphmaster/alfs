#python pass 2

#version 3.8.2

python-2 : openssl
	@$(extract)
	./configure --prefix=/usr --enable-shared --with-system-expat --with-system-ffi --with-ensurepip=yes
	$(MAKE)
	$(MAKE) install
	chmod -v 755 /usr/lib/libpython3.8.so
	chmod -v 755 /usr/lib/libpython3.so
	ln -sfv pip3.8 /usr/bin/pip3
	#install -v -dm755 /usr/share/doc/python-3.8.2/html
	#tar --strip-components=1 --no-same-owner --no-same-permissions -C /usr/share/doc/python-3.8.2/html -xvf ../python-3.8.2-docs-html.tar.bz2
	$(remove)
	touch tmp/$(@F)
