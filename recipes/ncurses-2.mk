#ncurses pass 2

#version 6.2

ncurses-2 : pkg-config
	@$(extract)
	sed -i '/LIBTOOL_INSTALL/d' c++/Makefile.in
	./configure --prefix=/usr --mandir=/usr/share/man --with-shared --without-debug --without-normal --enable-pc-files --enable-widec
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/lib/libncursesw.so.6* /lib
	ln -sfv /lib/$$(readlink /usr/lib/libncursesw.so) /usr/lib/libncursesw.so
	for lib in ncurses form panel menu
	do
	    rm -vf /usr/lib/lib$${lib}.so &&
	    echo "INPUT(-l$${lib}w)" > /usr/lib/lib$${lib}.so &&
	    ln -sfv $${lib}w.pc /usr/lib/pkgconfig/$${lib}.pc || exit 1
	done
	rm -vf /usr/lib/libcursesw.so
	echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
	ln -sfv libncurses.so /usr/lib/libcurses.so
	mkdir -pv /usr/share/doc/ncurses-6.2
	cp -rv doc/* /usr/share/doc/ncurses-6.2
	$(remove)
	touch tmp/$(@F)
