#make pass 1

#version 4.3

make-1 : gzip-1
	@$(extract)
	./configure --prefix=/tools --without-guile
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
