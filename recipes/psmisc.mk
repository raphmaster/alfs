#psmisc

#version 23.3

psmisc : sed-2
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/bin/fuser /bin
	mv -vf /usr/bin/killall /bin
	$(remove)
	touch tmp/$(@F)
