#patch pass 2

#version 2.7.6

patch-2 : make-2
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
