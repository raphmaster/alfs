#zstd

#version 1.4.4

zstd : gzip-2
	@$(extract)
	$(MAKE)
	$(MAKE) prefix=/usr install
	rm -v /usr/lib/libzstd.a
	mv -v /usr/lib/libzstd.so.* /lib
	ln -sfv /lib/$$(readlink /usr/lib/libzstd.so) /usr/lib/libzstd.so
	$(remove)
	touch tmp/$(@F)
