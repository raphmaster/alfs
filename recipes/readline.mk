#readline

#version 8.0

readline : file-2
	@$(extract)
	sed -i '/MV.*old/d' Makefile.in
	sed -i '/{OLDSUFF}/c:' support/shlib-install
	./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/readline-8.0
	$(MAKE) SHLIB_LIBS="-L/tools/lib -lncursesw"
	$(MAKE) SHLIB_LIBS="-L/tools/lib -lncursesw" install
	mv -vf /usr/lib/lib{readline,history}.so.* /lib
	chmod -v u+w /lib/lib{readline,history}.so.*
	ln -sfv /lib/$$(readlink /usr/lib/libreadline.so) /usr/lib/libreadline.so
	ln -sfv /lib/$$(readlink /usr/lib/libhistory.so) /usr/lib/libhistory.so
	install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/readline-8.0
	$(remove)
	touch tmp/$(@F)
