#findutils pass 2

#version 4.7.0

findutils-2 : gawk-2
	@$(extract)
	./configure --prefix=/usr --localstatedir=/var/lib/locate
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/bin/find /bin
	sed -i 's|find:=$${BINDIR}|find:=/bin|' /usr/bin/updatedb
	$(remove)
	touch tmp/$(@F)
