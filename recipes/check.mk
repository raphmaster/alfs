#check

#version 0.14.0

check : coreutils-2
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) docdir=/usr/share/doc/check-0.14.0 install
	sed -i '1 s/tools/usr/' /usr/bin/checkmk
	$(remove)
	touch tmp/$(@F)
