#dejagnu

#version 1.6.2

dejagnu : expect
	@$(extract)
	./configure --prefix=/tools
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
