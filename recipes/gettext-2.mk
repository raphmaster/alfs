#gettext pass 2

#version 0.20.1

gettext-2 : kmod
	@$(extract)
	./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/gettext-0.20.1
	$(MAKE)
	$(MAKE) install
	chmod -v 0755 /usr/lib/preloadable_libintl.so
	$(remove)
	touch tmp/$(@F)
