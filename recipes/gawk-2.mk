#gawk pass 2

#version 5.0.1

gawk-2 : diffutils-2
	@$(extract)
	sed -i 's/extras//' Makefile.in
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	mkdir -pv /usr/share/doc/gawk-5.0.1
	cp -v doc/{awkforai.txt,*.{eps,pdf,jpg}} /usr/share/doc/gawk-5.0.1
	$(remove)
	touch tmp/$(@F)
