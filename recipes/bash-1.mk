#bash pass 1

#version 5.0

bash-1 : ncurses-1
	@$(extract)
	./configure --prefix=/tools --without-bash-malloc
	$(MAKE)
	$(MAKE) install
	ln -svf bash /tools/bin/sh
	$(remove)
	touch tmp/$(@F)
