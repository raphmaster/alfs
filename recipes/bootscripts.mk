#bootscripts

#version 20191031

bootscripts : cleanup
	@$(extract)
	#patch -Np1 -i $(CURDIR)/patches/lfs-bootscripts-20191031.patch
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
