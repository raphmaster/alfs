#expect

#version 5.45.4

expect : tcl
	@$(extract)
	cp -v configure{,.orig}
	sed -i 's:/usr/local/bin:/bin:' configure
	./configure --prefix=/tools --with-tcl=/tools/lib --with-tclinclude=/tools/include
	$(MAKE)
	$(MAKE) SCRIPTS="" install
	$(remove)
	touch tmp/$(@F)
