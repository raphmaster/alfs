#sed pass 1

#version 4.8

sed-1 : python-1
	@$(extract)
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
