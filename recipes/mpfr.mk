#mpfr

#version 4.0.2

mpfr : gmp
	@$(extract)
	./configure --prefix=/usr --disable-static --enable-thread-safe --docdir=/usr/share/doc/mpfr-4.0.2
	$(MAKE)
	$(MAKE) html
	$(MAKE) check
	read -p 'is mpfr test result correct? (y/n) '
	if [ $$REPLY != 'y' ]; then exit 1; fi
	$(MAKE) install
	$(MAKE) install-html
	$(remove)
	touch tmp/$(@F)
