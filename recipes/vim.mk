#vim

#version 8.2.0190

vim : texinfo-2
	@$(extract)
	echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	ln -svf vim /usr/bin/vi
	for L in /usr/share/man/{,*/}man1/vim.1
	do
	    ln -svf vim.1 $$(dirname $$L)/vi.1 || exit 1
	done
	ln -svf /usr/share/vim/vim82/doc /usr/share/doc/vim-8.2.0190
	cp -v $(CURDIR)/etc/vimrc /etc/vimrc
	$(remove)
	touch tmp/$(@F)
