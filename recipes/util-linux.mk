#util-linux

#version 2.35.1

util-linux : procps-ng
	@$(extract)
	mkdir -pv /var/lib/hwclock
	./configure ADJTIME_PATH=/var/lib/hwclock/adjtime --docdir=/usr/share/doc/util-linux-2.35.1 --disable-chfn-chsh --disable-login --disable-nologin --disable-su --disable-setpriv --disable-runuser --disable-pylibmount --disable-static --without-python --without-systemd --without-systemdsystemunitdir
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
