#glibc pass 1

#version 2.31

glibc-1 : linux-1
	@$(extract)
	mkdir -pv build
	pushd build
	../configure --prefix=/tools --host=$$LFS_TGT --build=$$(../scripts/config.guess) --enable-kernel=3.2 --with-headers=/tools/include
	$(MAKE)
	$(MAKE) install
	echo 'int main(){}' > dummy.c
	$${LFS_TGT}-gcc dummy.c
	if [[ ! $$(readelf -l a.out | grep ': /tools') =~ /tools/lib64/ld-linux-x86-64\.so\.2 ]]; then echo 'Toolchain is not working as expected!'; exit 1; fi
	rm -v dummy.c a.out
	$(remove)
	touch tmp/$(@F)
