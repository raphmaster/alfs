#xml-parser

#version 2.46

xml-parser : perl-2
	@$(extract)
	perl Makefile.PL
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
