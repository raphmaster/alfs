#automake

#version 1.16.1

automake : autoconf
	@$(extract)
	./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.1
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
