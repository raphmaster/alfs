#gperf

#version 3.1

gperf : gdbm
	@$(extract)
	./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
