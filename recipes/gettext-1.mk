#gettext pass 1

#version 0.20.1

gettext-1 : gawk-1
	@$(extract)
	./configure --disable-shared
	$(MAKE)
	cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /tools/bin
	$(remove)
	touch tmp/$(@F)
