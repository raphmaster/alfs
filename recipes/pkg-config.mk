#pkg-config

#version 0.29.2

pkg-config : gcc-3
	@$(extract)
	./configure --prefix=/usr --with-internal-glib --disable-host-tool --docdir=/usr/share/doc/pkg-config-0.29.2
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
