#binutils pass 3

#version 2.34

binutils-3 : bc
	@$(extract)
	sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in
	mkdir -pv build
	pushd build
	if [[ ! $$(expect -c "spawn ls") =~ spawn\ ls ]]; then echo 'Environment is not set up for proper PTY operation!'; exit 1; fi
	../configure --prefix=/usr --enable-gold --enable-ld=default --enable-plugins --enable-shared --disable-werror --enable-64-bit-bfd --with-system-zlib
	$(MAKE) tooldir=/usr
	$(MAKE) -k check || true
	read -p 'is binutils test result correct? (y/n) '
	if [ $$REPLY != 'y' ]; then exit 1; fi
	$(MAKE) tooldir=/usr install
	$(remove)
	touch tmp/$(@F)
