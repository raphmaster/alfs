#config

#version 20200318

config : bootscripts
	@$(exit?)
	cp -v $(CURDIR)/etc/sysconfig/{static.ifconfig.example,clock,console,rc.site} /etc/sysconfig/
	cp -v $(CURDIR)/etc/{resolv.conf,hostname,hosts,inittab,profile,inputrc,shells,fstab,lfs-release,lsb-release} /etc/
	touch tmp/$(@F)
