#grub

#version 2.04

grub : groff
	@$(extract)
	./configure --prefix=/usr --sbindir=/sbin --sysconfdir=/etc --disable-efiemu --disable-werror
	$(MAKE)
	$(MAKE) install
	mv -vf /etc/bash_completion.d/grub /usr/share/bash-completion/completions
	$(remove)
	touch tmp/$(@F)
