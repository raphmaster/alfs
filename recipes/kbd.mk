#kbd

#version 2.2.0

kbd : iproute2
	@$(extract)
	patch -Np1 -i ../kbd-2.2.0-backspace-1.patch
	sed -i 's/\(RESIZECONS_PROGS=\)yes/\1no/g' configure
	sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in
	export PKG_CONFIG_PATH=/tools/lib/pkgconfig
	./configure --prefix=/usr --disable-vlock
	$(MAKE)
	$(MAKE) install
	#mkdir -pv /usr/share/doc/kbd-2.2.0
	#cp -rv docs/doc/* /usr/share/doc/kbd-2.2.0
	$(remove)
	touch tmp/$(@F)
