#binutils pass 1

#version 2.34

binutils-1 : download
	@$(extract)
	mkdir -pv build
	pushd build
	../configure --prefix=/tools --with-sysroot=$(wdir) --with-lib-path=/tools/lib --target=$$LFS_TGT --disable-nls --disable-werror
	$(MAKE)
	mkdir -pv /tools/lib
	ln -svf lib /tools/lib64
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
