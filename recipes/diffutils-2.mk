#diffutils pass 1

#version 3.7

diffutils-2 : check
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
