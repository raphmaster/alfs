#build directories

#version 20200318

dirs :
	@mkdir -pv $(wdir)/{sources,tools} tmp
	#create tools directory for building temporary system
	echo 'asking root privileges to create link /tools -> $(wdir)/tools'
	sudo ln -Tsvi $(wdir)/tools /tools
	touch tmp/$(@F)
