#openssl

#version 1.1.1d

openssl : libffi
	@$(extract)
	./config --prefix=/usr --openssldir=/etc/ssl --libdir=lib shared zlib-dynamic
	$(MAKE)
	sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
	$(MAKE) MANSUFFIX=ssl install
	#mv -vf /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1d
	#cp -vfr doc/* /usr/share/doc/openssl-1.1.1d
	$(remove)
	touch tmp/$(@F)
