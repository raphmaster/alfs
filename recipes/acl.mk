#acl

#version 2.2.53

acl : attr
	@$(extract)
	./configure --prefix=/usr --bindir=/bin --disable-static --libexecdir=/usr/lib --docdir=/usr/share/doc/acl-2.2.53
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/lib/libacl.so.* /lib
	ln -sfv /lib/$$(readlink /usr/lib/libacl.so) /usr/lib/libacl.so
	$(remove)
	touch tmp/$(@F)
