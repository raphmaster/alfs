#texinfo pass 1

#version 6.7

texinfo-1 : tar-1
	@$(extract)
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
