#grep pass 2

#version 3.4

grep-2 : flex
	@$(extract)
	./configure --prefix=/usr --bindir=/bin
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
