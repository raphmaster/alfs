#inetutils

#version 1.9.4

inetutils : expat
	@$(extract)
	./configure --prefix=/usr --localstatedir=/var --disable-logger --disable-whois --disable-rcp --disable-rexec --disable-rlogin --disable-rsh --disable-servers
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/bin/{hostname,ping,ping6,traceroute} /bin
	mv -vf /usr/bin/ifconfig /sbin
	$(remove)
	touch tmp/$(@F)
