#gmp

#version 6.2.0

gmp : binutils-3
	@$(extract)
	#generic libraries can be created instead of optimized ones for the host
	#cp -v configfsf.guess config.guess
	#cp -v configfsf.sub config.sub
	./configure --prefix=/usr --enable-cxx --disable-static --docdir=/usr/share/doc/gmp-6.2.0
	$(MAKE)
	$(MAKE) html
	$(MAKE) check 2>&1 | tee gmp-check-log
	if [[ ! $$(awk '/# PASS:/{total+=$$3} ; END{print total}' gmp-check-log) =~ 197 ]]; then echo "$(@F) test suite failed!"; exit 1; fi
	$(MAKE) install
	$(MAKE) install-html
	$(remove)
	touch tmp/$(@F)
