#ncurses pass 1

#version 6.2

ncurses-1 : m4-1
	@$(extract)
	sed -i s/mawk// configure
	./configure --prefix=/tools --with-shared --without-debug --without-ada --enable-widec --enable-overwrite
	$(MAKE)
	$(MAKE) install
	ln -sv libncursesw.so /tools/lib/libncurses.so
	$(remove)
	touch tmp/$(@F)
