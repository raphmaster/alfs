#tar pass 2

#version 1.32

tar-2 : man-db
	@$(extract)
	export FORCE_UNSAFE_CONFIGURE=1
	./configure --prefix=/usr --bindir=/bin
	$(MAKE)
	$(MAKE) install
	$(MAKE) -C doc install-html docdir=/usr/share/doc/tar-1.32
	$(remove)
	touch tmp/$(@F)
