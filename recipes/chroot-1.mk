#chroot pass 1 (lfs)

#version 20200318

chroot-1 : | vkfs
	@#will tell sub-make that this target has been done
	touch tmp/$(@F)
	#copy alfs directory to new root (working directory)
	#package download urls file and all other necessary files must be in alfs directory
	mkdir -pv $(wdir)/$(notdir $(CURDIR))
	echo 'asking root privileges to copy alfs files to working directory'
	sudo cp -av makefile recipes utils tmp etc docs license readme patches $(urls) $(wdir)/$(notdir $(CURDIR))
	#enter chroot and continue executing make. on failure, save exit status for later
	echo 'asking root privileges to enter chroot'
	sudo chroot $(wdir) /tools/bin/env -i HOME=/root TERM="$(TERM)" PS1='\u:\w\$$ ' PATH=/bin:/usr/bin:/sbin:/usr/sbin:/tools/bin make -C $(notdir $(CURDIR)) $${MAKEFLAGS/--jobserver-auth=[0-9],[0-9]} $(chroot-1-goal) wdir=/ || status=$$?
	#update built targets from working directory
	cp -av $(wdir)/$(notdir $(CURDIR))/tmp .
	#remove target file and exit if chroot failed
	if [ -v status ]; then
	     rm -v $(wdir)/$(notdir $(CURDIR))/tmp/$(@F) tmp/$(@F)
	     exit $$status
	fi
