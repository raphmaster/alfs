#libtool

#version 2.4.6

libtool : bash-2
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
