#xz pass 1

#version 5.2.4

xz-1 : texinfo-1
	@$(extract)
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
