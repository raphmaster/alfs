#linux pass 2

#version 5.5.9

linux-2 : essentials
	@$(extract)
	$(MAKE) mrproper
	$(MAKE) headers
	find usr/include -name '.*' -delete
	rm -v usr/include/Makefile
	cp -rv usr/include/* /usr/include
	$(remove)
	touch tmp/$(@F)
