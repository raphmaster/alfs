#sysklogd

#version 1.5.1

sysklogd : e2fsprogs
	@$(extract)
	sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
	sed -i 's/union wait/int/' syslogd.c
	$(MAKE)
	$(MAKE) BINDIR=/sbin install
	cp -v $(CURDIR)/etc/syslog.conf /etc/syslog.conf
	$(remove)
	touch tmp/$(@F)
