#coreutils pass 1

#version 8.32

coreutils-1 : bzip2-1
	@$(extract)
	./configure --prefix=/tools --enable-install-program=hostname
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
