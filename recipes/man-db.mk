#man-db

#version 2.9.1

man-db : patch-2
	@$(extract)
	./configure --prefix=/usr --docdir=/usr/share/doc/man-db-2.9.1 --sysconfdir=/etc --disable-setuid --enable-cache-owner=bin --with-browser=/usr/bin/lynx --with-vgrind=/usr/bin/vgrind --with-grap=/usr/bin/grap --with-systemdtmpfilesdir= --with-systemdsystemunitdir=
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
