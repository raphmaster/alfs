#m4 pass 2

#version 1.4.18

m4-2 : readline
	@$(extract)
	sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
	echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
