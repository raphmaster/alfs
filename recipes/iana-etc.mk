#iana-etc

#version 2.30

iana-etc : psmisc
	@$(extract)
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
