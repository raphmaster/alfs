#strip pass 2

#version 20200318

strip-2 : eudev
	@$(exit?)
	#keep debugging information for regression tests that use valgring or gdb
	for lib in /lib/{ld-2.31.so,libc-2.31.so,libpthread-2.31.so,libthread_db-1.0.so} /usr/lib/{libquadmath.so.0.0.0,libstdc++.so.6.0.28,libitm.so.1.0.0,libatomic.so.1.2.0}
	do
	    objcopy --only-keep-debug $$lib $${lib}.dbg &&
	    strip --strip-unneeded $$lib &&
	    objcopy --add-gnu-debuglink=$${lib}.dbg $$lib || exit 1
	done
	#do not strip binaries that are running, so execute binaries from /tools
	/tools/bin/find /usr/lib -type f -name \*.a -exec /tools/bin/strip --strip-debug {} ';'
	/tools/bin/find /lib /usr/lib -type f \( -name \*.so* -a ! -name \*dbg \) -exec /tools/bin/strip --strip-unneeded {} ';'
	/tools/bin/find /{bin,sbin} /usr/{bin,sbin,libexec} -type f -exec /tools/bin/strip --strip-all {} ';'
	touch tmp/$(@F)
