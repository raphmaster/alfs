#perl pass 1

#version 5.30.1

perl-1 : patch-1
	@$(extract)
	sh Configure -des -Dprefix=/tools -Dlibs=-lm -Uloclibpth -Ulocincpth
	#make -jN causes resource temporarily unavailable on my system, -j runs fine
	$(MAKE) -j
	cp -v perl cpan/podlators/scripts/pod2man /tools/bin
	mkdir -pv /tools/lib/perl5/5.30.1
	cp -Rv lib/* /tools/lib/perl5/5.30.1
	$(remove)
	touch tmp/$(@F)
