#libcap

#version 2.33

libcap : ncurses-2
	@$(extract)
	sed -i '/install.*STACAPLIBNAME/d' libcap/Makefile
	$(MAKE) lib=lib
	$(MAKE) lib=lib install
	chmod -v 755 /lib/libcap.so.2.33
	mv -v /lib/libpsx.a /usr/lib
	rm -v /lib/libcap.so
	ln -sfv /lib/libcap.so.2 /usr/lib/libcap.so
	$(remove)
	touch tmp/$(@F)
