#binutils pass 2

#version 2.34

binutils-2 : libstdc++
	@$(extract)
	mkdir -pv build
	pushd build
	export CC=$${LFS_TGT}-gcc AR=$${LFS_TGT}-ar RANLIB=$${LFS_TGT}-ranlib
	../configure --prefix=/tools --disable-nls --disable-werror --with-lib-path=/tools/lib --with-sysroot
	$(MAKE)
	$(MAKE) install
	make -C ld clean
	make -C ld LIB_PATH=/usr/lib:/lib
	cp -v ld/ld-new /tools/bin
	$(remove)
	touch tmp/$(@F)
