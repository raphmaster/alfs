#tar pass 1

#version 1.32

tar-1 : sed-1
	@$(extract)
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
