#xz pass 2

#version 5.2.4

xz-2 : bzip2-2
	@$(extract)
	./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/xz-5.2.4
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat} /bin
	mv -vf /usr/lib/liblzma.so.* /lib
	ln -svf /lib/$$(readlink /usr/lib/liblzma.so) /usr/lib/liblzma.so
	$(remove)
	touch tmp/$(@F)
