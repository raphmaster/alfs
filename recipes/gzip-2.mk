#gzip pass 2

#version 1.10

gzip-2 : less
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/bin/gzip /bin
	$(remove)
	touch tmp/$(@F)
