#libpipeline

#version 1.5.2

libpipeline : kbd
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
