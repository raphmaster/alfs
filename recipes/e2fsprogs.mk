#e2fsprogs

#version 1.45.5

e2fsprogs : util-linux
	@$(extract)
	mkdir -pv build
	pushd build
	../configure --prefix=/usr --bindir=/bin --with-root-prefix="" --enable-elf-shlibs --disable-libblkid --disable-libuuid --disable-uuidd --disable-fsck
	$(MAKE)
	$(MAKE) install
	chmod -v u+w /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
	gunzip -v /usr/share/info/libext2fs.info.gz
	install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info
	#makeinfo -o doc/com_err.info ../lib/et/com_err.texinfo
	#install -v -m644 doc/com_err.info /usr/share/info
	#install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info
	$(remove)
	touch tmp/$(@F)
