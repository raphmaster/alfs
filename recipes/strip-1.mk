#strip pass 1

#version 20200318

strip-1 : xz-1
	@#strip libraries, do not stop on non-zero exit status
	strip --strip-debug /tools/lib/* || true
	#strip binaries
	/usr/bin/strip --strip-unneeded /tools/{,s}bin/* || true
	#remove documentation
	rm -rvf /tools/{,share}/{info,man,doc}
	#remove unnedded files
	find /tools/{lib,libexec} -name \*.la -delete
	touch tmp/$(@F)
