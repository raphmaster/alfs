#attr

#version 2.4.48

attr : mpc
	@$(extract)
	./configure --prefix=/usr --bindir=/bin --disable-static --sysconfdir=/etc --docdir=/usr/share/doc/attr-2.4.48
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/lib/libattr.so.* /lib
	ln -sfv /lib/$$(readlink /usr/lib/libattr.so) /usr/lib/libattr.so
	$(remove)
	touch tmp/$(@F)
