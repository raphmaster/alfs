#zlib

#version 1.2.11

zlib : adjusting
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/lib/libz.so.* /lib
	ln -sfv /lib/$$(readlink /usr/lib/libz.so) /usr/lib/libz.so
	$(remove)
	touch tmp/$(@F)
