#gcc pass 3

#version 9.3.0

gcc-3 : shadow
	@$(extract)
	sed -i -e '/m64=/s/lib64/lib/' gcc/config/i386/t-linux64 #change 64-bit libraries default directory to lib
	#official documentation recommends building in a dedicated directory
	mkdir -pv build
	pushd build
	export SED=sed
	../configure --prefix=/usr --enable-languages=c,c++ --disable-multilib --disable-bootstrap --with-system-zlib
	$(MAKE)
	ulimit -s 32768
	chown -Rv nobody .
	su nobody -s /bin/bash -c "PATH=$$PATH $(MAKE) -k check" || true
	../contrib/test_summary | grep -A7 Summ
	echo 'test summary can be compared with those located at http://www.linuxfromscratch.org/lfs/build-logs/9.0/'
	read -p 'is the above test summary correct? (y/n) '
	if [ $$REPLY != 'y' ]; then exit 1; fi
	$(MAKE) install
	rm -rvf /usr/lib/gcc/$$(gcc -dumpmachine)/9.3.0/include-fixed/bits/
	chown -Rv root:root /usr/lib/gcc/*linux-gnu/9.3.0/include{,-fixed}
	ln -svf /usr/bin/cpp /lib
	ln -svf gcc /usr/bin/cc
	install -v -dm755 /usr/lib/bfd-plugins
	ln -sfv /usr/libexec/gcc/$$(gcc -dumpmachine)/9.3.0/liblto_plugin.so /usr/lib/bfd-plugins/
	echo 'int main(){}' > dummy.c
	cc dummy.c -v -Wl,--verbose &> dummy.log
	if [[ ! $$(readelf -l a.out | grep ': /lib') =~ /lib64/ld-linux-x86-64\.so\.2 ]]; then echo 'Toolchain is not working as expected!'; exit 1; fi
	if [[ ! $$(grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log) =~ /usr/lib/gcc/x86_64-pc-linux-gnu/9\.3\.0.*/crt[1in]\.o\ succeeded[[:space:]]*{3} ]]; then echo 'Incorrect start files!'; exit 1; fi
	if [[ ! $$(grep -B4 '^ /usr/include' dummy.log) =~ \#include\ \<\.\.\.\>\ search\ starts\ here:[[:space:]]*/usr/lib/gcc/x86_64-pc-linux-gnu/9\.3\.0/include[[:space:]]*/usr/local/include[[:space:]]*/usr/lib/gcc/x86_64-pc-linux-gnu/9\.3\.0/include-fixed[[:space:]]*/usr/include ]]; then echo 'Incorrect compiler header files!'; exit 1; fi
	if [[ ! $$(grep 'SEARCH.*/usr/lib' dummy.log | sed 's|; |\n|g') =~ SEARCH_DIR\(\"/usr/x86_64-pc-linux-gnu/lib64\"\)[[:space:]]SEARCH_DIR\(\"/usr/local/lib64\"\)[[:space:]]SEARCH_DIR\(\"/lib64\"\)[[:space:]]SEARCH_DIR\(\"/usr/lib64\"\)[[:space:]]SEARCH_DIR\(\"/usr/x86_64-pc-linux-gnu/lib\"\)[[:space:]]SEARCH_DIR\(\"/usr/local/lib\"\)[[:space:]]SEARCH_DIR\(\"/lib\"\)[[:space:]]SEARCH_DIR\(\"/usr/lib\"\)\; ]]; then echo 'Incorrect linker search paths!'; exit 1; fi
	if [[ ! $$(grep "/lib.*/libc.so.6 " dummy.log) =~ attempt\ to\ open\ /lib/libc\.so\.6\ succeeded ]]; then echo 'Incorrect libc!'; exit 1; fi
	if [[ ! $$(grep found dummy.log) =~ found\ ld-linux-x86-64\.so\.2\ at\ /lib/ld-linux-x86-64\.so\.2 ]]; then echo 'Incorrect GCC dynamic linker!'; exit 1; fi
	rm -v dummy.c a.out dummy.log
	mkdir -pv /usr/share/gdb/auto-load/usr/lib
	mv -vf /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib
	$(remove)
	touch tmp/$(@F)
