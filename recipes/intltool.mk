#intltool

#version 0.51.0

intltool : xml-parser
	@$(extract)
	sed -i 's:\\\$${:\\\$$\\{:' intltool-update.in
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO
	$(remove)
	touch tmp/$(@F)
