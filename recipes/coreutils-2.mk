#coreutils pass 2

#version 8.32

coreutils-2 : meson
	@$(extract)
	patch -Np1 -i ../coreutils-8.32-i18n-1.patch
	sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk
	autoreconf -fiv
	export FORCE_UNSAFE_CONFIGURE=1
	./configure --prefix=/usr --enable-no-install-program=kill,uptime
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
	mv -vf /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
	mv -vf /usr/bin/{rmdir,stty,sync,true,uname} /bin
	mv -vf /usr/bin/chroot /usr/sbin
	mv -vf /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
	sed -i s/\"1\"/\"8\"/1 /usr/share/man/man8/chroot.8
	mv -vf /usr/bin/{head,sleep,nice,touch} /bin
	$(remove)
	touch tmp/$(@F)
