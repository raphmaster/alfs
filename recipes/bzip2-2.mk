#bzip2 pass 2

#version 1.0.8

bzip2-2 : zlib
	@$(extract)
	patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch
	sed -i 's@\(ln -s -f \)$$(PREFIX)/bin/@\1@' Makefile
	sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
	$(MAKE) -f Makefile-libbz2_so
	$(MAKE) clean
	$(MAKE)
	$(MAKE) PREFIX=/usr install
	cp -v bzip2-shared /bin/bzip2
	cp -av libbz2.so* /lib
	ln -svf /lib/libbz2.so.1.0 /usr/lib/libbz2.so
	rm -v /usr/bin/{bunzip2,bzcat,bzip2}
	ln -svf bzip2 /bin/bunzip2
	ln -svf bzip2 /bin/bzcat
	$(remove)
	touch tmp/$(@F)
