#procps-ng

#version 3.3.16

procps-ng : vim
	@$(extract)
	./configure --prefix=/usr --exec-prefix= --libdir=/usr/lib --docdir=/usr/share/doc/procps-ng-3.3.16 --disable-static --disable-kill
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/lib/libprocps.so.* /lib
	ln -sfv /lib/$$(readlink /usr/lib/libprocps.so) /usr/lib/libprocps.so
	$(remove)
	touch tmp/$(@F)
