#texinfo pass 2

#version 6.7

texinfo-2 : tar-2
	@$(extract)
	./configure --prefix=/usr --disable-static
	$(MAKE)
	$(MAKE) install
	#$(MAKE) TEXMF=/usr/share/texmf install-tex
	$(remove)
	touch tmp/$(@F)
