#meson

#version 0.53.2

meson : ninja
	@$(extract)
	python3 setup.py build
	python3 setup.py install --root=dest
	cp -rv dest/* /
	$(remove)
	touch tmp/$(@F)
