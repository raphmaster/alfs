#make pass 2

#version 4.3

make-2 : libpipeline
	@$(extract)
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
