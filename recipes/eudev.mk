#eudev

#version 3.2.9

eudev : sysvinit
	@$(extract)
	./configure --prefix=/usr --bindir=/sbin --sbindir=/sbin --libdir=/usr/lib --sysconfdir=/etc --libexecdir=/lib --with-rootprefix= --with-rootlibdir=/lib --enable-manpages --disable-static
	$(MAKE)
	mkdir -pv /lib/udev/rules.d
	mkdir -pv /etc/udev/rules.d
	$(MAKE) install
	tar -xvf ../udev-lfs-20171102.tar.xz
	$(MAKE) -f udev-lfs-20171102/Makefile.lfs install
	#this command need to be run each time hardware information is updated
	udevadm hwdb --update
	$(remove)
	touch tmp/$(@F)
