#findutils pass 1

#version 4.7.0

findutils-1 : file-1
	@$(extract)
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
