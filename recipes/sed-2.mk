#sed pass 2

#version 4.8

sed-2 : libcap
	@$(extract)
	sed -i 's/usr/tools/' build-aux/help2man
	sed -i 's/testsuite.panic-tests.sh//' Makefile.in
	./configure --prefix=/usr --bindir=/bin
	$(MAKE)
	$(MAKE) html
	$(MAKE) install
	install -d -m755 /usr/share/doc/sed-4.8
	install -m644 doc/sed.html /usr/share/doc/sed-4.8
	$(remove)
	touch tmp/$(@F)
