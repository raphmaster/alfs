#libffi

#version 3.3

libffi : libelf
	@$(extract)
	#you can change --with-gcc-arch= parameter to an alternative system type, see x86 options in the gcc manual
	./configure --prefix=/usr --disable-static --with-gcc-arch=native
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
