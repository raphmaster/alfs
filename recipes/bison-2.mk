#bison pass 2

#version 3.5.3

bison-2 : iana-etc
	@$(extract)
	./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.5.3
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
