#tcl

#version 8.6.10

tcl : gcc-2
	@$(extract)
	pushd unix
	./configure --prefix=/tools
	$(MAKE)
	$(MAKE) install
	chmod -v u+w /tools/lib/libtcl8.6.so
	$(MAKE) install-private-headers
	ln -svf tclsh8.6 /tools/bin/tclsh
	$(remove)
	touch tmp/$(@F)
