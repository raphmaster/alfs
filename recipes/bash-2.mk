#bash pass 2

#version 5.0

bash-2 : grep-2
	@$(extract)
	patch -Np1 -i ../bash-5.0-upstream_fixes-1.patch
	./configure --prefix=/usr --docdir=/usr/share/doc/bash-5.0 --without-bash-malloc --with-installed-readline
	$(MAKE)
	$(MAKE) install
	mv -vf /usr/bin/bash /bin
	$(remove)
	touch tmp/$(@F)
