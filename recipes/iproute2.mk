#iproute2

#version 5.5.0

iproute2 : zstd
	@$(extract)
	sed -i /ARPD/d Makefile
	rm -fv man/man8/arpd.8
	sed -i 's/.m_ipt.o//' tc/Makefile
	$(MAKE)
	$(MAKE) DOCDIR=/usr/share/doc/iproute2-5.5.0 install
	$(remove)
	touch tmp/$(@F)
