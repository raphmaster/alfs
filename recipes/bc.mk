#bc

#version 2.6.0

bc : m4-2
	@$(extract)
	export PREFIX=/usr CC=gcc CFLAGS="-std=c99"
	./configure.sh -G -O3
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
