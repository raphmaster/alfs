#gcc pass 2

#version 9.3.0

gcc-2 : binutils-2
	@$(extract)
	cat gcc/limitx.h gcc/glimits.h gcc/limity.h > $$(dirname $$($${LFS_TGT}-gcc -print-libgcc-file-name))/include-fixed/limits.h
	#unpack mpfr,gmp,mpc and rename them so gcc build will use them
	tar -xvf ../$$($(call find_archive_func,mpfr))
	mv -vf $$($(call tar_root_func,mpfr)) mpfr
	tar -xvf ../$$($(call find_archive_func,gmp))
	mv -vf $$($(call tar_root_func,gmp)) gmp
	tar -xvf ../$$($(call find_archive_func,mpc))
	mv -vf $$($(call tar_root_func,mpc)) mpc
	#change default dynamic linker to the one in tools
	#remove /usr/include from include search path
	for file in gcc/config/{linux,i386/linux{,64}}.h
	do
	    cp -v $${file}{,.orig} && #backup files
	    sed -i -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' -e 's@/usr@/tools@g' $$file && #prepend /tools to /lib/ld, /lib64/ld and /lib32/ld and replace /usr with /tools
	    echo '
	#undef STANDARD_STARTFILE_PREFIX_1
	#undef STANDARD_STARTFILE_PREFIX_2
	#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
	#define STANDARD_STARTFILE_PREFIX_2 ""' >> $$file || exit 1
	done
	sed -i -e '/m64=/s/lib64/lib/' gcc/config/i386/t-linux64 #change 64-bit libraries default directory to lib
	#official documentation recommends building in a dedicated directory
	mkdir -pv build
	pushd build
	export CC=$${LFS_TGT}-gcc CXX=$${LFS_TGT}-g++ AR=$${LFS_TGT}-ar RANLIB=$${LFS_TGT}-ranlib
	../configure --prefix=/tools --with-local-prefix=/tools --with-native-system-header-dir=/tools/include --enable-languages=c,c++ --disable-libstdcxx-pch --disable-multilib --disable-bootstrap --disable-libgomp
	$(MAKE)
	$(MAKE) install
	ln -svf gcc /tools/bin/cc
	echo 'int main(){}' > dummy.c
	cc dummy.c
	if [[ ! $$(readelf -l a.out | grep ': /tools') =~ /tools/lib64/ld-linux-x86-64\.so\.2 ]]; then echo 'Toolchain is not working as expected!'; exit 1; fi
	rm -v dummy.c a.out
	$(remove)
	touch tmp/$(@F)
