#mpc

#version 1.1.0

mpc : mpfr
	@$(extract)
	./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/mpc-1.1.0
	$(MAKE)
	$(MAKE) html
	$(MAKE) install
	$(MAKE) install-html
	$(remove)
	touch tmp/$(@F)
