#autoconf

#version 2.69

autoconf : intltool
	@$(extract)
	sed -i '361 s/{/\\{/' bin/autoscan.in
	./configure --prefix=/usr
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
