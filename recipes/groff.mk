#groff

#version 1.22.4

groff : findutils-2
	@$(extract)
	#set paper size (letter,A4)
	PAGE=letter ./configure --prefix=/usr
	$(MAKE) -j 1
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
