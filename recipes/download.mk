#download packages

#version 20200318

download : version-check
	@sed 's/#.*//' $(urls) | cut -f 2- | tr '\t' '\n' | wget -i - -nc -P $(wdir)/sources
	touch tmp/$(@F)
