#flex

#version 2.6.4

flex : bison-2
	@$(extract)
	sed -i "/math.h/a #include <malloc.h>" src/flexdef.h
	export HELP2MAN=/tools/bin/true
	./configure --prefix=/usr --docdir=/usr/share/doc/flex-2.6.4
	$(MAKE)
	$(MAKE) install
	ln -svf flex /usr/bin/lex
	$(remove)
	touch tmp/$(@F)
