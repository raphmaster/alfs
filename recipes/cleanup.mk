#clean

#version 20200318

cleanup : strip-2
	@$(exit?)
	rm -rvf /tmp/*
	rm -vf /usr/lib/lib{bfd,opcodes}.a
	rm -vf /usr/lib/libbz2.a
	rm -vf /usr/lib/lib{com_err,e2p,ext2fs,ss}.a
	rm -vf /usr/lib/libltdl.a
	rm -vf /usr/lib/libfl.a
	rm -vf /usr/lib/libz.a
	find /usr/lib /usr/libexec -name \*.la -delete
	touch tmp/$(@F)
