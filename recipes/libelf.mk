#libelf

#version 0.178

libelf : gettext-2
	@$(exit?)
	$(call extract_func,elfutils)
	./configure --prefix=/usr --disable-debuginfod
	$(MAKE)
	$(MAKE) -C libelf install
	install -vm644 config/libelf.pc /usr/lib/pkgconfig
	rm -v /usr/lib/libelf.a
	$(call remove_func,elfutils)
	touch tmp/$(@F)
