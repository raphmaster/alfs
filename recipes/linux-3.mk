#linux pass 3

#version 5.5.9

linux-3 : config
	@$(extract)
	$(MAKE) mrproper
	$(MAKE) defconfig
	$(MAKE) menuconfig
	$(MAKE)
	#make -j $procs modules_install && #install modules (in /lib/modules) if you use them in your configuration
	cp -iv arch/x86/boot/bzImage /boot/vmlinuz-5.5.9 #assuming x86 architecture
	cp -iv System.map /boot/System.map-5.5.9 #install map file
	cp -iv .config /boot/config-5.5.9 #keep config for future reference
	install -d /usr/share/doc/linux-5.5.9
	cp -rv Documentation/* /usr/share/doc/linux-5.5.9
	$(remove)
	touch tmp/$(@F)
