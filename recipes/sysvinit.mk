#sysvinit

#version 2.96

sysvinit : sysklogd
	@$(extract)
	patch -Np1 -i ../sysvinit-2.96-consolidated-1.patch
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
