#shadow

#version 4.8.1

shadow : acl
	@$(extract)
	sed -i 's/groups$$(EXEEXT) //' src/Makefile.in
	find man -name Makefile.in -exec sed -i 's/groups\.1 / /' {} \;
	find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
	find man -name Makefile.in -exec sed -i 's/passwd\.5 / /' {} \;
	sed -i -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD SHA512@' -e 's@/var/spool/mail@/var/mail@' etc/login.defs
	sed -i 's/1000/999/' etc/useradd
	./configure --sysconfdir=/etc --with-group-name-max-length=32
	$(MAKE)
	$(MAKE) install
	pwconv
	grpconv
	sed -i 's/yes/no/' /etc/default/useradd
	#set password for root
	passwd root
	$(remove)
	touch tmp/$(@F)
