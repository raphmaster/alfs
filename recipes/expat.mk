#expat

#version 2.2.9

expat : gperf
	@$(extract)
	sed -i 's|usr/bin/env |bin/|' run.sh.in
	./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/expat-2.2.9
	$(MAKE)
	$(MAKE) install
	#install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.9
	$(remove)
	touch tmp/$(@F)
