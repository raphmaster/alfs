#virtual kernel filesystems

#version 20200318

vkfs : strip-1
	@#change owner of tools to root to prevent user from altering it
	echo 'asking root privileges to change owner of tools to root'
	sudo chown -Rv root: $(wdir)/tools
	#create directories of virtual kernel filesystems
	echo 'asking root privileges to create virtual kernel filesystems directories'
	sudo mkdir -pv $(wdir)/{dev,proc,sys,run}
	#create required devices nodes only if not already created
	if [ ! -c $(wdir)/dev/console ]; then
	    echo 'asking root privileges to create console device node'
	    sudo mknod -m 600 $(wdir)/dev/console c 5 1
	fi
	if [ ! -c $(wdir)/dev/null ]; then
	    echo 'asking root privileges to create null device node'
	    sudo mknod -m 666 $(wdir)/dev/null c 1 3
	fi
	#bind mount /dev from host
	echo 'asking root privileges to bind mount /dev to $(wdir)/dev'
	sudo mount -v --bind /dev $(wdir)/dev
	echo 'asking root privileges to mount devpts to $(wdir)/dev/pts'
	sudo mount -vt devpts devpts $(wdir)/dev/pts -o gid=5,mode=620
	echo 'asking root privileges to mount proc to $(wdir)/proc'
	sudo mount -vt proc proc $(wdir)/proc
	echo 'asking root privileges to mount sys to $(wdir)/sys'
	sudo mount -vt sysfs sysfs $(wdir)/sys
	echo 'asking root privileges to mount tmpfs to $(wdir)/run'
	sudo mount -vt tmpfs tmpfs $(wdir)/run
	#create a directory if a symbolic link that point to it exists
	if [ -h $(wdir)/dev/shm ]; then
	    echo 'asking root privileges to create directory pointed by /dev/shm'
	    sudo mkdir -pv $(wdir)/$$(readlink $(wdir)/dev/shm)
	fi
	touch tmp/$(@F)
