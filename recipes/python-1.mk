#python pass 1

#version 3.8.2

python-1 : perl-1
	@$(extract)
	sed -i '/def add_multiarch_path/a \        return' setup.py
	./configure --prefix=/tools --without-ensurepip
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
