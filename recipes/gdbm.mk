#gdbm

#version 1.18.1

gdbm : libtool
	@$(extract)
	./configure --prefix=/usr --disable-static --enable-libgdbm-compat
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
