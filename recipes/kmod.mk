#kmod

#version 27

kmod : automake
	@$(extract)
	./configure --prefix=/usr --bindir=/bin --sysconfdir=/etc --with-rootlibdir=/lib --with-xz --with-zlib
	$(MAKE)
	$(MAKE) install
	for target in depmod insmod lsmod modinfo modprobe rmmod
	do
	    ln -sfv /bin/kmod /sbin/$$target || exit 1
	done
	ln -sfv kmod /bin/lsmod
	$(remove)
	touch tmp/$(@F)
