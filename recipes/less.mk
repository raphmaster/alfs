#less

#version 551

less : grub
	@$(extract)
	./configure --prefix=/usr --sysconfdir=/etc
	$(MAKE)
	$(MAKE) install
	$(remove)
	touch tmp/$(@F)
