#Automated Linux From Scratch

#version 20200318

all : linux-3

clean :
	@if [ -d tmp ]; then
	    rm -rv tmp
	fi
	if [ -d $(wdir) ]; then
	    sudo rm -rvf $(wdir)
	fi

#include all necessary makefiles
include utils/lib.mk recipes/*.mk
.PHONY : all clean
#search directories for targets and prerequisites
VPATH += tmp/
#all recipe commands are executed in one sub-shell
.ONESHELL :
#exit immediately if a command exits with a non-zero status and always search in PATH to run a program
.SHELLFLAGS += -e +h
#use bash shell
export SHELL := bash
#working directory where all packages will be built
wdir := build
#file that contains urls to download packages
urls := urls
#get absolute name of working directory and urls file
#if wdir is /, empty wdir
override wdir := $(shell echo $(abspath $(wdir)) | sed 's@^/$$@@')
override urls := $(abspath $(urls))
#do not pass down urls or wdir variables if they are supplied as command line arguments
MAKEOVERRIDES:=$(patsubst urls=,,$(MAKEOVERRIDES))
MAKEOVERRIDES:=$(patsubst wdir=,,$(MAKEOVERRIDES))
#target recipes from binutils-1 to xz-1 need special environment
binutils-1 gcc-1 linux-1 glibc-1 libstdc++ binutils-2 gcc-2 tcl expect dejagnu m4-1 ncurses-1 bash-1 bison-1 bzip2-1 coreutils-1 diffutils-1 file-1 findutils-1 gawk-1 gettext-1 grep-1 gzip-1 make-1 patch-1 perl-1 python-1 sed-1 tar-1 texinfo-1 xz-1 : BASH_ENV := utils/tools-env
#reset BASH_ENV for recipes before binutils-1
dirs version-check download : BASH_ENV :=
#export BASH_ENV so make put it in environment for bash
export BASH_ENV
#list of lfs targets in order (chroot-1 must only accept these targets)
lfs-targets := essentials linux-2 man-pages glibc-2 adjusting zlib bzip2-2 xz-2 file-2 readline m4-2 bc binutils-3 gmp mpfr mpc attr acl shadow gcc-3 pkg-config ncurses-2 libcap sed-2 psmisc iana-etc bison-2 flex grep-2 bash-2 libtool gdbm gperf expat inetutils perl-2 xml-parser intltool autoconf automake kmod gettext-2 libelf libffi openssl python-2 ninja meson coreutils-2 check diffutils-2 gawk-2 findutils-2 groff grub less gzip-2 zstd iproute2 kbd libpipeline make-2 patch-2 man-db tar-2 texinfo-2 vim procps-ng util-linux e2fsprogs sysklogd sysvinit eudev strip-2 cleanup bootscripts config linux-3
#pass only lfs targets to chroot-1
chroot-1-goal := $(filter $(lfs-targets),$(MAKECMDGOALS))
#if no goal for chroot-1 command default to the last lfs target
ifeq ($(chroot-1-goal),)
    chroot-1-goal := linux-3
endif
